﻿/*

Copyright 2017 Vitaliy Urusovskij <vurusovskij_505@mail.ru>, Grigoriy Rudnij <rudnyjjg@rambler.ru>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

 */

namespace IRS
{
	partial class frm_Main
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Main));
            this.pb_Logo = new System.Windows.Forms.PictureBox();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.cms_Tray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cms_Tray_Item_Show = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.cms_Tray_Item_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_Open = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Add = new System.Windows.Forms.Button();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.btn_Start = new System.Windows.Forms.Button();
            this.dgv_Configuration = new System.Windows.Forms.DataGridView();
            this.col_HandGesturesL = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.col_HandGesturesR = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.col_Action = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.col_Repeat = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dia_Open = new System.Windows.Forms.OpenFileDialog();
            this.dia_Save = new System.Windows.Forms.SaveFileDialog();
            this.gb_Mode = new System.Windows.Forms.GroupBox();
            this.btn_file = new System.Windows.Forms.Button();
            this.rb_Playback = new System.Windows.Forms.RadioButton();
            this.rb_Live = new System.Windows.Forms.RadioButton();
            this.btn_Stop = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Logo)).BeginInit();
            this.cms_Tray.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Configuration)).BeginInit();
            this.gb_Mode.SuspendLayout();
            this.SuspendLayout();
            // 
            // pb_Logo
            // 
            this.pb_Logo.Image = global::IRS.Properties.Resources.logo;
            this.pb_Logo.Location = new System.Drawing.Point(5, 7);
            this.pb_Logo.Margin = new System.Windows.Forms.Padding(4);
            this.pb_Logo.Name = "pb_Logo";
            this.pb_Logo.Size = new System.Drawing.Size(233, 44);
            this.pb_Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pb_Logo.TabIndex = 0;
            this.pb_Logo.TabStop = false;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.cms_Tray;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // cms_Tray
            // 
            this.cms_Tray.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cms_Tray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cms_Tray_Item_Show,
            this.toolStripMenuItem1,
            this.cms_Tray_Item_Exit});
            this.cms_Tray.Name = "cms_Tray";
            this.cms_Tray.Size = new System.Drawing.Size(157, 58);
            // 
            // cms_Tray_Item_Show
            // 
            this.cms_Tray_Item_Show.Name = "cms_Tray_Item_Show";
            this.cms_Tray_Item_Show.Size = new System.Drawing.Size(156, 24);
            this.cms_Tray_Item_Show.Text = "Развернуть";
            this.cms_Tray_Item_Show.Click += new System.EventHandler(this.ExpandToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(153, 6);
            // 
            // cms_Tray_Item_Exit
            // 
            this.cms_Tray_Item_Exit.Name = "cms_Tray_Item_Exit";
            this.cms_Tray_Item_Exit.Size = new System.Drawing.Size(156, 24);
            this.cms_Tray_Item_Exit.Text = "Выход";
            this.cms_Tray_Item_Exit.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // btn_Open
            // 
            this.btn_Open.Location = new System.Drawing.Point(5, 357);
            this.btn_Open.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Open.Name = "btn_Open";
            this.btn_Open.Size = new System.Drawing.Size(109, 41);
            this.btn_Open.TabIndex = 4;
            this.btn_Open.Text = "Open";
            this.btn_Open.UseVisualStyleBackColor = true;
            this.btn_Open.Click += new System.EventHandler(this.btn_Open_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(123, 357);
            this.btn_Save.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(109, 41);
            this.btn_Save.TabIndex = 5;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Add
            // 
            this.btn_Add.Location = new System.Drawing.Point(436, 357);
            this.btn_Add.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(109, 41);
            this.btn_Add.TabIndex = 7;
            this.btn_Add.Text = "Add";
            this.btn_Add.UseVisualStyleBackColor = true;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // btn_Delete
            // 
            this.btn_Delete.Location = new System.Drawing.Point(553, 357);
            this.btn_Delete.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(109, 41);
            this.btn_Delete.TabIndex = 8;
            this.btn_Delete.Text = "Del";
            this.btn_Delete.UseVisualStyleBackColor = true;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // btn_Start
            // 
            this.btn_Start.Location = new System.Drawing.Point(671, 260);
            this.btn_Start.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(167, 42);
            this.btn_Start.TabIndex = 9;
            this.btn_Start.Text = "Start";
            this.btn_Start.UseVisualStyleBackColor = true;
            this.btn_Start.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // dgv_Configuration
            // 
            this.dgv_Configuration.AllowUserToAddRows = false;
            this.dgv_Configuration.AllowUserToDeleteRows = false;
            this.dgv_Configuration.AllowUserToResizeColumns = false;
            this.dgv_Configuration.AllowUserToResizeRows = false;
            this.dgv_Configuration.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Configuration.BackgroundColor = System.Drawing.Color.White;
            this.dgv_Configuration.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgv_Configuration.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Configuration.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_HandGesturesL,
            this.col_HandGesturesR,
            this.col_Action,
            this.col_Repeat});
            this.dgv_Configuration.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv_Configuration.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv_Configuration.Location = new System.Drawing.Point(5, 69);
            this.dgv_Configuration.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_Configuration.MultiSelect = false;
            this.dgv_Configuration.Name = "dgv_Configuration";
            this.dgv_Configuration.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgv_Configuration.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_Configuration.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgv_Configuration.ShowCellErrors = false;
            this.dgv_Configuration.ShowCellToolTips = false;
            this.dgv_Configuration.ShowEditingIcon = false;
            this.dgv_Configuration.ShowRowErrors = false;
            this.dgv_Configuration.Size = new System.Drawing.Size(657, 281);
            this.dgv_Configuration.TabIndex = 10;
            // 
            // col_HandGesturesL
            // 
            this.col_HandGesturesL.HeaderText = "Left_Hand";
            this.col_HandGesturesL.Items.AddRange(new object[] {
            "-",
            "cursor_click",
            "cursor_clockwise_circle",
            "cursor_counter_clockwise_circle",
            "cursor_hand_opening",
            "cursor_hand_closing",
            "click",
            "fist",
            "full_pinch",
            "spreadfingers",
            "swipe_down",
            "swipe_left",
            "swipe_right",
            "swipe_up",
            "tap",
            "thumb_down",
            "thumb_up",
            "two_fingers_pinch_open",
            "v_sign",
            "wave"});
            this.col_HandGesturesL.Name = "col_HandGesturesL";
            this.col_HandGesturesL.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // col_HandGesturesR
            // 
            this.col_HandGesturesR.HeaderText = "Right_Hand";
            this.col_HandGesturesR.Items.AddRange(new object[] {
            "-",
            "cursor_click",
            "cursor_clockwise_circle",
            "cursor_counter_clockwise_circle",
            "cursor_hand_opening",
            "cursor_hand_closing",
            "click",
            "fist",
            "full_pinch",
            "spreadfingers",
            "swipe_down",
            "swipe_left",
            "swipe_right",
            "swipe_up",
            "tap",
            "thumb_down",
            "thumb_up",
            "two_fingers_pinch_open",
            "v_sign",
            "wave"});
            this.col_HandGesturesR.Name = "col_HandGesturesR";
            // 
            // col_Action
            // 
            this.col_Action.HeaderText = "Action";
            this.col_Action.Items.AddRange(new object[] {
            "-",
            "ArrowUp",
            "ArrowDown",
            "ArrowLeft",
            "ArrowRight",
            "Space",
            "Enter",
            "Ctrl",
            "PrtScr",
            "Shift",
            "Tab",
            "CapsLock",
            "NumLock",
            "Del",
            "Backspace",
            "Escape",
            "Home",
            "End",
            "PageUp",
            "PageDown",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "0",
            "Num1",
            "Num2",
            "Num3",
            "Num4",
            "Num5",
            "Num6",
            "Num7",
            "Num8",
            "Num9",
            "Num0",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z",
            "Play/Pause",
            "Stop",
            "Next",
            "Prev",
            "VolumeUp",
            "VolumeDown",
            "Mute",
            "Mouse_Left_Pressed",
            "Mouse_Left_Release",
            "Mouse_Left_Click",
            "Mouse_Left_DoubleClick",
            "Mouse_Right_Pressed",
            "Mouse_Right_Release",
            "Mouse_Right_Click",
            "Mouse_Middle_Pressed",
            "Mouse_Middle_Release",
            "Mouse_Middle_Click",
            "Mouse_Wheel_Up",
            "Mouse_Wheel_Down"});
            this.col_Action.Name = "col_Action";
            this.col_Action.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // col_Repeat
            // 
            this.col_Repeat.HeaderText = "Continuous";
            this.col_Repeat.Name = "col_Repeat";
            this.col_Repeat.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dia_Open
            // 
            this.dia_Open.Filter = "Файлы конфигурации|*.xml";
            this.dia_Open.Title = "Открыть";
            // 
            // dia_Save
            // 
            this.dia_Save.Filter = "Файлы конфигурации|*.xml";
            this.dia_Save.Title = "Сохранить";
            // 
            // gb_Mode
            // 
            this.gb_Mode.Controls.Add(this.btn_file);
            this.gb_Mode.Controls.Add(this.rb_Playback);
            this.gb_Mode.Controls.Add(this.rb_Live);
            this.gb_Mode.Location = new System.Drawing.Point(671, 69);
            this.gb_Mode.Margin = new System.Windows.Forms.Padding(4);
            this.gb_Mode.Name = "gb_Mode";
            this.gb_Mode.Padding = new System.Windows.Forms.Padding(4);
            this.gb_Mode.Size = new System.Drawing.Size(167, 89);
            this.gb_Mode.TabIndex = 11;
            this.gb_Mode.TabStop = false;
            this.gb_Mode.Text = "Mode";
            // 
            // btn_file
            // 
            this.btn_file.Location = new System.Drawing.Point(107, 48);
            this.btn_file.Margin = new System.Windows.Forms.Padding(4);
            this.btn_file.Name = "btn_file";
            this.btn_file.Size = new System.Drawing.Size(51, 31);
            this.btn_file.TabIndex = 2;
            this.btn_file.Text = "...";
            this.btn_file.UseVisualStyleBackColor = true;
            this.btn_file.Click += new System.EventHandler(this.btn_file_Click);
            // 
            // rb_Playback
            // 
            this.rb_Playback.AutoSize = true;
            this.rb_Playback.Location = new System.Drawing.Point(12, 53);
            this.rb_Playback.Margin = new System.Windows.Forms.Padding(4);
            this.rb_Playback.Name = "rb_Playback";
            this.rb_Playback.Size = new System.Drawing.Size(86, 21);
            this.rb_Playback.TabIndex = 1;
            this.rb_Playback.TabStop = true;
            this.rb_Playback.Text = "Playback";
            this.rb_Playback.UseVisualStyleBackColor = true;
            this.rb_Playback.CheckedChanged += new System.EventHandler(this.rb_Playback_CheckedChanged);
            // 
            // rb_Live
            // 
            this.rb_Live.AutoSize = true;
            this.rb_Live.Checked = true;
            this.rb_Live.Location = new System.Drawing.Point(12, 26);
            this.rb_Live.Margin = new System.Windows.Forms.Padding(4);
            this.rb_Live.Name = "rb_Live";
            this.rb_Live.Size = new System.Drawing.Size(55, 21);
            this.rb_Live.TabIndex = 0;
            this.rb_Live.TabStop = true;
            this.rb_Live.Text = "Live";
            this.rb_Live.UseVisualStyleBackColor = true;
            // 
            // btn_Stop
            // 
            this.btn_Stop.Enabled = false;
            this.btn_Stop.Location = new System.Drawing.Point(671, 309);
            this.btn_Stop.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Stop.Name = "btn_Stop";
            this.btn_Stop.Size = new System.Drawing.Size(167, 41);
            this.btn_Stop.TabIndex = 12;
            this.btn_Stop.Text = "Stop";
            this.btn_Stop.UseVisualStyleBackColor = true;
            this.btn_Stop.Click += new System.EventHandler(this.btn_Stop_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(845, 18);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(399, 372);
            this.textBox1.TabIndex = 14;
            this.textBox1.Text = "Log";
            // 
            // frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(845, 405);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btn_Stop);
            this.Controls.Add(this.gb_Mode);
            this.Controls.Add(this.dgv_Configuration);
            this.Controls.Add(this.btn_Start);
            this.Controls.Add(this.btn_Delete);
            this.Controls.Add(this.btn_Add);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.btn_Open);
            this.Controls.Add(this.pb_Logo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "frm_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gesture Control Service";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Main_FormClosing);
            this.Resize += new System.EventHandler(this.frm_Main_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pb_Logo)).EndInit();
            this.cms_Tray.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Configuration)).EndInit();
            this.gb_Mode.ResumeLayout(false);
            this.gb_Mode.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pb_Logo;
		private System.Windows.Forms.NotifyIcon notifyIcon1;
		private System.Windows.Forms.ContextMenuStrip cms_Tray;
		private System.Windows.Forms.ToolStripMenuItem cms_Tray_Item_Exit;
		private System.Windows.Forms.ToolStripMenuItem cms_Tray_Item_Show;
		private System.Windows.Forms.Button btn_Open;
		private System.Windows.Forms.Button btn_Save;
		private System.Windows.Forms.Button btn_Add;
		private System.Windows.Forms.Button btn_Delete;
		private System.Windows.Forms.Button btn_Start;
		private System.Windows.Forms.DataGridView dgv_Configuration;
		private System.Windows.Forms.OpenFileDialog dia_Open;
		private System.Windows.Forms.SaveFileDialog dia_Save;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.GroupBox gb_Mode;
        private System.Windows.Forms.RadioButton rb_Playback;
        private System.Windows.Forms.RadioButton rb_Live;
        private System.Windows.Forms.Button btn_Stop;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btn_file;
        private System.Windows.Forms.DataGridViewComboBoxColumn col_HandGesturesL;
        private System.Windows.Forms.DataGridViewComboBoxColumn col_HandGesturesR;
        private System.Windows.Forms.DataGridViewComboBoxColumn col_Action;
        private System.Windows.Forms.DataGridViewCheckBoxColumn col_Repeat;
    }
}

