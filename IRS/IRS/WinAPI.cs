﻿/*

Copyright 2017 Vitaliy Urusovskij <vurusovskij_505@mail.ru>, Grigoriy Rudnij <rudnyjjg@rambler.ru>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IRS
{
	class WinAPI
	{
        [DllImport("user32.dll", SetLastError = true)]
        public static extern void mouse_event(uint dwFlags, uint x, uint y, uint data, int extra);

        public void MouseLeftPressed()
        {
            mouse_event(0x00000002, 0, 0, 0, 0);
        }

        public void MouseRightPressed()
        {
            mouse_event(0x00000008, 0, 0, 0, 0);
        }

        public void MouseLeftRelease()
        {
            mouse_event(0x00000004, 0, 0, 0, 0);
        }

        public void MouseLeftClick()
        {
            mouse_event(0x00000002, 0, 0, 0, 0);
            mouse_event(0x00000004, 0, 0, 0, 0);
        }

        public void MouseLeftDoubleClick()
        {
            mouse_event(0x00000002, 0, 0, 0, 0);
            mouse_event(0x00000004, 0, 0, 0, 0);

            mouse_event(0x00000002, 0, 0, 0, 0);
            mouse_event(0x00000004, 0, 0, 0, 0);
        }

        public void MouseRightClick()
        {
            mouse_event(0x00000008, 0, 0, 0, 0);
            mouse_event(0x00000010, 0, 0, 0, 0);
        }

        public void MouseRightRelease()
        {
            mouse_event(0x00000010, 0, 0, 0, 0);
        }

        public void MouseMiddlePressed()
        {
            mouse_event(0x00000020, 0, 0, 0, 0);
        }

        public void MouseMiddleRelease()
        {
            mouse_event(0x00000040, 0, 0, 0, 0);
        }

        [DllImport("user32.dll")]
        static extern uint keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        public static void KeyPress(System.Windows.Forms.Keys key)
        {
            // KeyUp
            keybd_event((byte)key, 0, 0, 0);

            // KeyDown
            keybd_event((byte)key, 0, 0x7F, 0);
        }

    }
}
