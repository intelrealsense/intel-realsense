﻿/*

Copyright 2017 Vitaliy Urusovskij <vurusovskij_505@mail.ru>, Grigoriy Rudnij <rudnyjjg@rambler.ru>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

 */

using System;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Collections.Generic;

namespace IRS
{
    public partial class frm_Main : Form
    {
        // To close Form
        private volatile bool closing = false;
        
        // It is necessary to stop recognition threads
        public volatile bool stop = false;

        private string filename = "";

        // It is necessary to remember the last gesture
        private string LastLeftGesture = "";
        private string LastRightGesture = "";

        // Gesures, that can be continuous
        private string[] ContinuousGestures = { "spreadfingers", "fist", "thumb_up", "thumb_down", "v_sign", "full_pinch" };

        // Recognition threads
        System.Threading.Thread CursorThread = null, HandThread = null;

        private WinAPI winApi = new WinAPI();

        // if changes in dgv don't save
        int XMLSaved = 1;  

        private void CreateXMLDocument(string filepath)
        {
            // create XML file
            XmlTextWriter Doc = new XmlTextWriter(filepath, Encoding.UTF8);
            Doc.WriteStartDocument();
            Doc.WriteStartElement("Rows");
            Doc.WriteEndDocument();
            Doc.Close();
        }

        private void WriteToXMLDocument(string filepath)
        {
            // write XML file
            XmlDocument Doc = new XmlDocument();
            FileStream fs = new FileStream(filepath, FileMode.Open);
            Doc.Load(fs);

            int RowsCount = dgv_Configuration.RowCount;
            int ColumnsCount = dgv_Configuration.ColumnCount;

            for (int i = 0; i < RowsCount; i++)
            {
                XmlElement Row = Doc.CreateElement("Row");
                for (int j = 0; j < ColumnsCount; j++)
                {
                    XmlElement ColumnHeader = Doc.CreateElement(dgv_Configuration.Columns[j].HeaderText);
                    XmlText Cell = Doc.CreateTextNode(dgv_Configuration.Rows[i].Cells[j].Value.ToString());
                    ColumnHeader.AppendChild(Cell);
                    Row.AppendChild(ColumnHeader);
                }
                Doc.DocumentElement.AppendChild(Row);
            }
            fs.Close();
            Doc.Save(filepath);
        }

        private void ReadXMLDocument(string filepath)
        {
            // clear dgv_Configuration
            dgv_Configuration.Rows.Clear();

            // read XML file
            XmlDocument Doc = new XmlDocument();
            FileStream fs = new FileStream(filepath, FileMode.Open);
            Doc.Load(fs);

            XmlNodeList List = Doc.GetElementsByTagName("Row");
            int ListCount = List.Count;
            int NodesCount = List[0].ChildNodes.Count;
            for (int i = 0; i < ListCount; i++)
            {
                dgv_Configuration.Rows.Add();
                for (int j = 0; j < NodesCount; j++)
                {
                    XmlElement elem = (XmlElement)Doc.GetElementsByTagName(dgv_Configuration.Columns[j].HeaderText)[i];
                    dgv_Configuration.Rows[i].Cells[j].Value = elem.InnerText;
                }
            }
            fs.Close();
        }
                
        public frm_Main()
        {
            InitializeComponent();
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            dgv_Configuration.Rows.Add("-", "-", "-", false);
            XMLSaved = 0;
        }

        private void btn_Delete_Click(object sender, EventArgs e)
        {
            if (dgv_Configuration.SelectedCells.Count == 0) return;
            int ind = dgv_Configuration.SelectedCells[0].RowIndex;
            dgv_Configuration.Rows.RemoveAt(ind);
            XMLSaved = 0;
        }

        public Dictionary<string, bool> ReturnDGVGestures()
        {
            var map = new Dictionary<string, bool>();
            for (int i = 0; i < dgv_Configuration.RowCount; i++)
            {
                //bool cell = dgv_Configuration.Rows[i].Cells[3].Value.ToString() == "True";
                if (!map.ContainsKey(dgv_Configuration.Rows[i].Cells[0].Value.ToString()))
                {
                    //map.Add(dgv_Configuration.Rows[i].Cells[0].Value.ToString(), cell);
                    map.Add(dgv_Configuration.Rows[i].Cells[0].Value.ToString(), true);
                }
                if (!map.ContainsKey(dgv_Configuration.Rows[i].Cells[1].Value.ToString()))
                {
                    //map.Add(dgv_Configuration.Rows[i].Cells[1].Value.ToString(), cell);
                    map.Add(dgv_Configuration.Rows[i].Cells[1].Value.ToString(), true);
                }
            }
            return map;
        }

        
        private delegate void UpdateInfoDelegate(string status);
        public void UpdateInfo(string status)
        {
            //textBox1.Invoke(new UpdateInfoDelegate(delegate (string s)
            Invoke(new UpdateInfoDelegate(delegate (string s)
            {
                /// Because some gestures activate actions twice
                /// we use LastLeftGesture && LastRightGesture.
                /// It needs to avoid repeats.

                // If Hand is out of borders or does not calibrated
                if (s[0] == '#')
                {
                    LastLeftGesture = LastRightGesture = "";
                    textBox1.Text = s + "\r\n" + textBox1.Text;
                    return;
                }
                int RowsCount = dgv_Configuration.RowCount;
                int ColumnsCount = dgv_Configuration.ColumnCount;
                string cell0, cell1, cell2, cell3;

                string hand = s.Split(' ')[0];
                string gesture = s.Split(' ')[1];

                for (int i = 0; i < RowsCount; i++)
                {
                    cell0 = dgv_Configuration.Rows[i].Cells[0].Value.ToString();
                    cell1 = dgv_Configuration.Rows[i].Cells[1].Value.ToString();
                    cell2 = dgv_Configuration.Rows[i].Cells[2].Value.ToString();
                    cell3 = dgv_Configuration.Rows[i].Cells[3].Value.ToString();

                    if (cell3 == "False" && gesture.Split('_')[0] != "cursor" && Array.IndexOf(ContinuousGestures, gesture) != -1)
                    {
                        if (hand == "L" && gesture == LastLeftGesture)
                        {
                            cell0 = "-";
                        }
                        if (hand == "R" && gesture == LastRightGesture)
                        {
                            cell1 = "-";
                        }
                    }

                    if (cell0 != "-" || cell1 != "-")
                        if ((hand == "L" && cell0 == gesture ) || (hand == "R" && cell1 == gesture ))
                        {
                            switch (cell2)
                            {
                                case "Escape": WinAPI.KeyPress(System.Windows.Forms.Keys.Escape); break;
                                case "PageDown": WinAPI.KeyPress(System.Windows.Forms.Keys.Next); break;
                                case "PageUp": WinAPI.KeyPress(System.Windows.Forms.Keys.Prior); break;
                                case "ArrowUp": WinAPI.KeyPress(System.Windows.Forms.Keys.Up); break;
                                case "ArrowDown": WinAPI.KeyPress(System.Windows.Forms.Keys.Down); break;
                                case "ArrowLeft": WinAPI.KeyPress(System.Windows.Forms.Keys.Left); break;
                                case "ArrowRight": WinAPI.KeyPress(System.Windows.Forms.Keys.Right); break;
                                case "Space": WinAPI.KeyPress(System.Windows.Forms.Keys.Space); break;
                                case "Enter": WinAPI.KeyPress(System.Windows.Forms.Keys.Enter); break;
                                case "Ctrl": WinAPI.KeyPress(System.Windows.Forms.Keys.Control); break;
                                case "PrtScr": WinAPI.KeyPress(System.Windows.Forms.Keys.PrintScreen); break;
                                case "Shift": WinAPI.KeyPress(System.Windows.Forms.Keys.Shift); break;
                                case "Tab": WinAPI.KeyPress(System.Windows.Forms.Keys.Tab); break;
                                case "CapsLock": WinAPI.KeyPress(System.Windows.Forms.Keys.Capital); break;
                                case "NumLock": WinAPI.KeyPress(System.Windows.Forms.Keys.NumLock); break;
                                case "Del": WinAPI.KeyPress(System.Windows.Forms.Keys.Delete); break;
                                case "Backspase": WinAPI.KeyPress(System.Windows.Forms.Keys.Back); break;
                                case "Home": WinAPI.KeyPress(System.Windows.Forms.Keys.Home); break;
                                case "End": WinAPI.KeyPress(System.Windows.Forms.Keys.End); break;
                                case "0": WinAPI.KeyPress(System.Windows.Forms.Keys.D0); break;
                                case "1": WinAPI.KeyPress(System.Windows.Forms.Keys.D1); break;
                                case "2": WinAPI.KeyPress(System.Windows.Forms.Keys.D2); break;
                                case "3": WinAPI.KeyPress(System.Windows.Forms.Keys.D3); break;
                                case "4": WinAPI.KeyPress(System.Windows.Forms.Keys.D4); break;
                                case "5": WinAPI.KeyPress(System.Windows.Forms.Keys.D5); break;
                                case "6": WinAPI.KeyPress(System.Windows.Forms.Keys.D6); break;
                                case "7": WinAPI.KeyPress(System.Windows.Forms.Keys.D7); break;
                                case "8": WinAPI.KeyPress(System.Windows.Forms.Keys.D8); break;
                                case "9": WinAPI.KeyPress(System.Windows.Forms.Keys.D9); break;
                                case "Num0": WinAPI.KeyPress(System.Windows.Forms.Keys.NumPad0); break;
                                case "Num1": WinAPI.KeyPress(System.Windows.Forms.Keys.NumPad1); break;
                                case "Num2": WinAPI.KeyPress(System.Windows.Forms.Keys.NumPad2); break;
                                case "Num3": WinAPI.KeyPress(System.Windows.Forms.Keys.NumPad3); break;
                                case "Num4": WinAPI.KeyPress(System.Windows.Forms.Keys.NumPad4); break;
                                case "Num5": WinAPI.KeyPress(System.Windows.Forms.Keys.NumPad5); break;
                                case "Num6": WinAPI.KeyPress(System.Windows.Forms.Keys.NumPad6); break;
                                case "Num7": WinAPI.KeyPress(System.Windows.Forms.Keys.NumPad7); break;
                                case "Num8": WinAPI.KeyPress(System.Windows.Forms.Keys.NumPad8); break;
                                case "Num9": WinAPI.KeyPress(System.Windows.Forms.Keys.NumPad9); break;
                                case "A": WinAPI.KeyPress(System.Windows.Forms.Keys.A); break;
                                case "B": WinAPI.KeyPress(System.Windows.Forms.Keys.B); break;
                                case "C": WinAPI.KeyPress(System.Windows.Forms.Keys.C); break;
                                case "D": WinAPI.KeyPress(System.Windows.Forms.Keys.D); break;
                                case "E": WinAPI.KeyPress(System.Windows.Forms.Keys.E); break;
                                case "F": WinAPI.KeyPress(System.Windows.Forms.Keys.F); break;
                                case "G": WinAPI.KeyPress(System.Windows.Forms.Keys.G); break;
                                case "H": WinAPI.KeyPress(System.Windows.Forms.Keys.H); break;
                                case "I": WinAPI.KeyPress(System.Windows.Forms.Keys.I); break;
                                case "J": WinAPI.KeyPress(System.Windows.Forms.Keys.J); break;
                                case "K": WinAPI.KeyPress(System.Windows.Forms.Keys.K); break;
                                case "L": WinAPI.KeyPress(System.Windows.Forms.Keys.L); break;
                                case "M": WinAPI.KeyPress(System.Windows.Forms.Keys.M); break;
                                case "N": WinAPI.KeyPress(System.Windows.Forms.Keys.N); break;
                                case "O": WinAPI.KeyPress(System.Windows.Forms.Keys.O); break;
                                case "P": WinAPI.KeyPress(System.Windows.Forms.Keys.P); break;
                                case "Q": WinAPI.KeyPress(System.Windows.Forms.Keys.Q); break;
                                case "R": WinAPI.KeyPress(System.Windows.Forms.Keys.R); break;
                                case "S": WinAPI.KeyPress(System.Windows.Forms.Keys.S); break;
                                case "T": WinAPI.KeyPress(System.Windows.Forms.Keys.T); break;
                                case "U": WinAPI.KeyPress(System.Windows.Forms.Keys.U); break;
                                case "V": WinAPI.KeyPress(System.Windows.Forms.Keys.V); break;
                                case "W": WinAPI.KeyPress(System.Windows.Forms.Keys.W); break;
                                case "X": WinAPI.KeyPress(System.Windows.Forms.Keys.X); break;
                                case "Y": WinAPI.KeyPress(System.Windows.Forms.Keys.Y); break;
                                case "Z": WinAPI.KeyPress(System.Windows.Forms.Keys.Z); break;
                                case "Play/Pause": WinAPI.KeyPress(System.Windows.Forms.Keys.MediaPlayPause); break;
                                case "Stop": WinAPI.KeyPress(System.Windows.Forms.Keys.MediaStop); break;
                                case "Next": WinAPI.KeyPress(System.Windows.Forms.Keys.MediaNextTrack); break;
                                case "Prev": WinAPI.KeyPress(System.Windows.Forms.Keys.MediaPreviousTrack); break;
                                case "VolumeUp": WinAPI.KeyPress(System.Windows.Forms.Keys.VolumeUp); break;
                                case "VolumeDown": WinAPI.KeyPress(System.Windows.Forms.Keys.VolumeDown); break;
                                case "Mute": WinAPI.KeyPress(System.Windows.Forms.Keys.VolumeMute); break;
                                case "Mouse_Left_Pressed": winApi.MouseLeftPressed(); break;
                                case "Mouse_Left_Release": winApi.MouseLeftRelease(); break;
                                case "Mouse_Right_Pressed": winApi.MouseRightPressed(); break;
                                case "Mouse_Right_Release": winApi.MouseRightRelease(); break;
                                case "Mouse_Middle_Pressed": winApi.MouseMiddlePressed(); break;
                                case "Mouse_Middle_Release": winApi.MouseMiddleRelease(); break;
                                case "Mouse_Left_Click": winApi.MouseLeftClick(); break;
                                case "Mouse_Left_DoubleClick": winApi.MouseLeftDoubleClick(); break;
                                case "Mouse_Right_Click": winApi.MouseRightClick(); break;
                            }

                            //textBox1.Text = s + "\r\n" + textBox1.Text;
                        }
                }

                if (hand == "L") LastLeftGesture = gesture;
                if (hand == "R") LastRightGesture = gesture;


                
            }), new object[] { status });
        }       

        void CursorRecognitionInitialize()
        {
            HandsRecognition CR = new HandsRecognition(this);
            CR.CursorPipeline();
        }

        void HandRecognitionInitialize()
        {
            HandsRecognition HR = new HandsRecognition(this);
            HR.HandPipeline();
        }

        delegate void DoRecognitionCompleted();

        private void btn_Start_Click(object sender, EventArgs e)
        {
            stop = false;
            btn_Start.Enabled = false;
            btn_Stop.Enabled = true;
            btn_file.Enabled = btn_Add.Enabled = btn_Delete.Enabled = btn_Open.Enabled = btn_Save.Enabled = dgv_Configuration.Enabled = false;

            System.Threading.Thread.Sleep(5);

            this.Invoke(new DoRecognitionCompleted(
                delegate
                {
                    if (closing)
                        Close();
                }
            ));

            try
            {
                CursorThread = new System.Threading.Thread(CursorRecognitionInitialize);
                CursorThread.Start();
                if (CursorThread.ThreadState == System.Threading.ThreadState.Running)
                {
                    HandThread = new System.Threading.Thread(HandRecognitionInitialize);
                    HandThread.Start();
                }
            }
            catch (AggregateException ae)
            {
                MessageBox.Show(ae.InnerException.Message);
            }

        }

        public bool GetPlaybackState()
        {
            return rb_Playback.Checked;
        }

        public string GetFileName()
        {
            return filename;
        }

        private void btn_Stop_Click(object sender, EventArgs e)
        {
            stop = true;
            System.Threading.Thread.Sleep(100);
            CursorThread.Abort();
            CursorThread.Join();
            HandThread.Abort();
            HandThread.Join();
            VisibleInterface();
        }

        
        public void VisibleInterface()
        {
            btn_Start.Enabled = true;
            btn_Stop.Enabled = false;
            btn_file.Enabled = btn_Add.Enabled = btn_Delete.Enabled = btn_Open.Enabled = btn_Save.Enabled = dgv_Configuration.Enabled = true;
        }

        private void btn_Open_Click(object sender, EventArgs e)
        {
            if (XMLSaved == 0)
            {
                if (MessageBox.Show("Changes don't save. Do you want to continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (dia_Open.ShowDialog() == DialogResult.OK)
                    {
                        // read XML file
                        ReadXMLDocument(dia_Open.FileName);
                        XMLSaved = 1;
                    }
                }
            }
            else
            {
                if (dia_Open.ShowDialog() == DialogResult.OK)
                {
                    // read XML file
                    ReadXMLDocument(dia_Open.FileName);
                }
            }
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            if (dia_Save.ShowDialog() == DialogResult.OK)
            {
                // create XML file
                CreateXMLDocument(dia_Save.FileName);

                //write XML file
                WriteToXMLDocument(dia_Save.FileName);
                XMLSaved = 1;
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.ShowInTaskbar = true;
            this.WindowState = FormWindowState.Normal;
        }

        private void ExpandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowInTaskbar = true;
            this.WindowState = FormWindowState.Normal;
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        void OpenFile()
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "RSSDK clip|*.rssdk|Old format clip|*.pcsdk|All files|*.*";
                ofd.CheckFileExists = true;
                ofd.CheckPathExists = true;
                filename = (ofd.ShowDialog() == DialogResult.OK) ? ofd.FileName : null;
                if (filename != null) rb_Playback.Checked = true;
                else rb_Live.Checked = true;
            }
        }

        private void btn_file_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        private void frm_Main_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
             this.ShowInTaskbar = false;
            else this.ShowInTaskbar = true;
        }

        private void frm_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (CursorThread != null) CursorThread.Abort();
            if (HandThread != null) HandThread.Abort();
            System.Threading.Thread.Sleep(20);
        }

        private void rb_Playback_CheckedChanged(object sender, EventArgs e)
        {
            if (!rb_Playback.Checked)
            {
                filename = "";
                return;
            }
            if (filename == "") OpenFile();
        }
    }
}
