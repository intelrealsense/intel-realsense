﻿/*

Copyright 2017 Vitaliy Urusovskij <vurusovskij_505@mail.ru>, Grigoriy Rudnij <rudnyjjg@rambler.ru>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

 */


using System;
using System.Windows.Forms;

namespace IRS
{
    class HandsRecognition
    {
        private readonly frm_Main _form;
        private PXCMSession session = null;
        
        PXCMCursorData.ICursor icursor = null;
        private const float CURSOR_CONST = 1.1f;

        public HandsRecognition(frm_Main form)
        {
            this._form = form;
            session = PXCMSession.CreateInstance();
        }

        /* Displaying current frame hand gestures */
        private void DisplayGesture(PXCMHandData handAnalysis, int frameNumber)
        {

            int firedGesturesNumber = handAnalysis.QueryFiredGesturesNumber();
            string gestureStatusLeft = string.Empty;
            string gestureStatusRight = string.Empty;

            if (firedGesturesNumber == 0)
            {
                return;
            }

            for (int i = 0; i < firedGesturesNumber; i++)
            {
                PXCMHandData.GestureData gestureData;
                
                if (handAnalysis.QueryFiredGestureData(i, out gestureData) == pxcmStatus.PXCM_STATUS_NO_ERROR)
                {
                    PXCMHandData.IHand handData;
                    if (handAnalysis.QueryHandDataById(gestureData.handId, out handData) != pxcmStatus.PXCM_STATUS_NO_ERROR)
                        return;

                    PXCMHandData.BodySideType bodySideType = handData.QueryBodySide();
                    if (bodySideType == PXCMHandData.BodySideType.BODY_SIDE_LEFT)
                    {
                        gestureStatusLeft = gestureData.name.ToLower();
                    }
                    else if (bodySideType == PXCMHandData.BodySideType.BODY_SIDE_RIGHT)
                    {
                        gestureStatusRight = gestureData.name.ToLower();
                    }

                    
                }

                if (gestureStatusLeft == String.Empty)
                    _form.UpdateInfo("R " + gestureStatusRight);
                else
                    _form.UpdateInfo("L " + gestureStatusLeft);

            }
            

        }

        /* Displaying current frame cursor gestures */
        private void DisplayCursorGesture(PXCMCursorData cursorAnalysis, int frameNumber)
        {
            int firedGesturesNumber = cursorAnalysis.QueryFiredGesturesNumber();
            string gestureStatusLeft = string.Empty;
            string gestureStatusRight = string.Empty;

            if (firedGesturesNumber == 0)
            {
                return;
            }

            for (int i = 0; i < firedGesturesNumber; i++)
            {
                PXCMCursorData.GestureData gestureData;
                if (cursorAnalysis.QueryFiredGestureData(i, out gestureData) == pxcmStatus.PXCM_STATUS_NO_ERROR)
                {
                    PXCMCursorData.ICursor cursorData;
                    if (cursorAnalysis.QueryCursorDataById(gestureData.handId, out cursorData) != pxcmStatus.PXCM_STATUS_NO_ERROR)
                        return;

                    PXCMCursorData.BodySideType bodySideType = cursorData.QueryBodySide();

                    string gestureName = string.Empty;
                    switch (gestureData.label)
                    {
                        case PXCMCursorData.GestureType.CURSOR_CLICK:
                            gestureName = "cursor_click";
                            break;
                        case PXCMCursorData.GestureType.CURSOR_CLOCKWISE_CIRCLE:
                            gestureName = "cursor_clockwise_circle";
                            break;
                        case PXCMCursorData.GestureType.CURSOR_COUNTER_CLOCKWISE_CIRCLE:
                            gestureName = "cursor_counter_clockwise_circle";
                            break;
                        case PXCMCursorData.GestureType.CURSOR_HAND_OPENING:
                            gestureName = "cursor_hand_opening";
                            break;
                        case PXCMCursorData.GestureType.CURSOR_HAND_CLOSING:
                            gestureName = "cursor_hand_closing";
                            break;
                    }

                    if (!string.IsNullOrEmpty(gestureName))
                    {
                        if (bodySideType == PXCMCursorData.BodySideType.BODY_SIDE_LEFT)
                        {
                            _form.UpdateInfo("L " + gestureName);
                        }
                        else if (bodySideType == PXCMCursorData.BodySideType.BODY_SIDE_RIGHT)
                        {
                            _form.UpdateInfo("R " + gestureName);
                        }
                    }
                }
            }
        }


        /* Displaying current frame hand alerts */
        private void DisplayAlerts(PXCMHandData handAnalysis, int frameNumber)
        {
            bool isChanged = false;
            string sAlert = "Alert: ";
            for (int i = 0; i < handAnalysis.QueryFiredAlertsNumber(); i++)
            {
                PXCMHandData.AlertData alertData;
                if (handAnalysis.QueryFiredAlertData(i, out alertData) != pxcmStatus.PXCM_STATUS_NO_ERROR)
                    continue;

                //See PXCMHandAnalysis.AlertData.AlertType for all available alerts
                switch (alertData.label)
                {
                    case PXCMHandData.AlertType.ALERT_HAND_DETECTED:
                        {

                            sAlert += "Hand Detected, ";
                            isChanged = true;
                            break;
                        }
                    case PXCMHandData.AlertType.ALERT_HAND_NOT_DETECTED:
                        {

                            sAlert += "Hand Not Detected, ";
                            isChanged = true;
                            break;
                        }
                    case PXCMHandData.AlertType.ALERT_HAND_CALIBRATED:
                        {

                            sAlert += "Hand Calibrated, ";
                            isChanged = true;
                            break;
                        }
                    case PXCMHandData.AlertType.ALERT_HAND_NOT_CALIBRATED:
                        {

                            sAlert += "Hand Not Calibrated, ";
                            isChanged = true;
                            break;
                        }
                    case PXCMHandData.AlertType.ALERT_HAND_INSIDE_BORDERS:
                        {

                            sAlert += "Hand Inside Border, ";
                            isChanged = true;
                            break;
                        }
                    case PXCMHandData.AlertType.ALERT_HAND_OUT_OF_BORDERS:
                        {

                            sAlert += "Hand Out Of Borders, ";
                            isChanged = true;
                            break;
                        }

                }
            }
            if (isChanged)
            {
                _form.UpdateInfo("#Frame " + frameNumber + ") " + sAlert + "\n");
            }
        }

        /* Displaying current frame cursor alerts */
        private void DisplayCursorAlerts(PXCMCursorData cursorAnalysis, int frameNumber)
        {
            bool isChanged = false;
            string sAlert = "Alert: ";
            for (int i = 0; i < cursorAnalysis.QueryFiredAlertsNumber(); i++)
            {
                PXCMCursorData.AlertData alertData;
                if (cursorAnalysis.QueryFiredAlertData(i, out alertData) != pxcmStatus.PXCM_STATUS_NO_ERROR)
                    continue;

                //See PXCMCursorData.AlertType for all available alerts
                switch (alertData.label)
                {

                    case PXCMCursorData.AlertType.CURSOR_DETECTED:
                        {
                            sAlert += "Cursor Detected, ";
                            isChanged = true;
                            break;
                        }
                    case PXCMCursorData.AlertType.CURSOR_NOT_DETECTED:
                        {
                            sAlert += "Cursor Not Detected, ";
                            isChanged = true;
                            break;
                        }
                    case PXCMCursorData.AlertType.CURSOR_INSIDE_BORDERS:
                        {
                            sAlert += "Cursor inside Borders, ";
                            isChanged = true;
                            break;
                        }
                    case PXCMCursorData.AlertType.CURSOR_OUT_OF_BORDERS:
                        {
                            sAlert += "Cursor Out Of Borders, ";
                            isChanged = true;
                            break;
                        }
                }
            }
            if (isChanged)
            {
                _form.UpdateInfo("Frame " + frameNumber + ") " + sAlert + "\n");
            }
        }

        public static pxcmStatus OnNewFrame(Int32 mid, PXCMBase module, PXCMCapture.Sample sample)
        {
            return pxcmStatus.PXCM_STATUS_NO_ERROR;
        }


        /* Using PXCMSenseManager to handle data */
        public void CursorPipeline()
        {
            bool liveCamera = false;
            PXCMSenseManager instance = null;

            instance = session.CreateSenseManager();
            if (instance == null)
            {
                return;
            }

            PXCMCaptureManager captureManager = instance.captureManager;

            if (captureManager != null)
            {
                if (_form.GetPlaybackState())
                {
                    captureManager.SetFileName(_form.GetFileName(), false);
                }
                else
                {
                    liveCamera = true;
                }
            }

            /* Set Module */

            PXCMHandCursorModule handCursorAnalysis = null;

            PXCMSenseManager.Handler handler = new PXCMSenseManager.Handler();
            handler.onModuleProcessedFrame = new PXCMSenseManager.Handler.OnModuleProcessedFrameDelegate(OnNewFrame);

            PXCMCursorConfiguration cursorConfiguration = null;
            PXCMCursorData cursorData = null;


            pxcmStatus status = instance.EnableHandCursor();
            handCursorAnalysis = instance.QueryHandCursor();

            if (status != pxcmStatus.PXCM_STATUS_NO_ERROR || handCursorAnalysis == null)
            {
                return;
            }
            cursorConfiguration = handCursorAnalysis.CreateActiveConfiguration();

            if (cursorConfiguration == null)
            {
                instance.Close();
                instance.Dispose();
                return;
            }
            cursorData = handCursorAnalysis.CreateOutput();
            if (cursorData == null)
            {
                instance.Close();
                instance.Dispose();
                return;
            }

            if (instance.Init(handler) == pxcmStatus.PXCM_STATUS_NO_ERROR)
            {
                PXCMCapture.DeviceInfo dinfo;
                PXCMCapture.DeviceModel dModel = PXCMCapture.DeviceModel.DEVICE_MODEL_SR300;
                PXCMCapture.Device device = instance.captureManager.device;
                if (device != null)
                {
                    device.QueryDeviceInfo(out dinfo);
                    dModel = dinfo.model;
                }

                if (cursorConfiguration != null)
                {
                    cursorConfiguration.EnableAllAlerts();
                    cursorConfiguration.ApplyChanges();
                }

                int frameCounter = 0;
                int frameNumber = 0;

                if (cursorConfiguration != null)
                {
                    cursorConfiguration.DisableAllGestures();
                    var map = _form.ReturnDGVGestures();
                    foreach (var row in map)
                    {
                        if (row.Key == "-") continue;
                        string curs = row.Key.Split('_')[0];
                        if (curs != "cursor") continue;
                        switch (row.Key)
                        {
                            case "cursor_click":
                                cursorConfiguration.EnableGesture(PXCMCursorData.GestureType.CURSOR_CLICK); break;
                            case "cursor_clockwise_circle":
                                cursorConfiguration.EnableGesture(PXCMCursorData.GestureType.CURSOR_CLOCKWISE_CIRCLE); break;
                            case "cursor_counter_clockwise_circle":
                                cursorConfiguration.EnableGesture(PXCMCursorData.GestureType.CURSOR_COUNTER_CLOCKWISE_CIRCLE); break;
                            case "cursor_hand_opening":
                                cursorConfiguration.EnableGesture(PXCMCursorData.GestureType.CURSOR_HAND_OPENING); break;
                            case "cursor_hand_closing":
                                cursorConfiguration.EnableGesture(PXCMCursorData.GestureType.CURSOR_HAND_CLOSING); break;
                        }
                    }

                    cursorConfiguration.ApplyChanges();
                }

                while (!_form.stop)
                {
                    if (instance.AcquireFrame(true) < pxcmStatus.PXCM_STATUS_NO_ERROR)
                    {
                        break;
                    }

                    frameCounter++;

                    if (instance.IsConnected())
                    {
                        PXCMCapture.Sample sample;
                        sample = instance.QueryHandCursorSample();
                        if (sample != null && sample.depth != null)
                        {
                            frameNumber = liveCamera ? frameCounter : instance.captureManager.QueryFrameIndex();

                            if (cursorData != null)
                            {
                                cursorData.Update();

                                cursorData.QueryCursorData(PXCMCursorData.AccessOrderType.ACCESS_ORDER_BY_TIME, 0, out icursor);

                                if (icursor != null)
                                {
                                    Cursor.Position = new System.Drawing.Point((int)((System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width * (0.5 - icursor.QueryAdaptivePoint().x) + System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width / 2) * CURSOR_CONST), (int)((System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height * (icursor.QueryAdaptivePoint().y - 0.5) + System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height / 2) * CURSOR_CONST));
                                }

                                DisplayCursorGesture(cursorData, frameNumber);
                                DisplayCursorAlerts(cursorData, frameNumber);
                            }

                        }
                    }
                    instance.ReleaseFrame();
                }
            }

            if (session != null)
                session.Dispose();

            // Clean Up
            if (cursorData != null) cursorData.Dispose();
            if (cursorConfiguration != null) cursorConfiguration.Dispose();

            instance.Close();
            instance.Dispose();
        }



        public void HandPipeline()
        {
            bool liveCamera = false;
            PXCMSenseManager instance = null;
            instance = session.CreateSenseManager(); 
            if (instance == null)
            {
                return;
            }

            PXCMCaptureManager captureManager = instance.captureManager;
            if (captureManager != null)
            {
                if (_form.GetPlaybackState())
                {
                    captureManager.SetFileName(_form.GetFileName(), false);
                }
                else
                {
                    liveCamera = true;
                }
            }

            /* Set Module */

            PXCMHandModule handAnalysis = null;

            PXCMSenseManager.Handler handler = new PXCMSenseManager.Handler();
            handler.onModuleProcessedFrame = new PXCMSenseManager.Handler.OnModuleProcessedFrameDelegate(OnNewFrame);

            PXCMHandConfiguration handConfiguration = null;
            PXCMHandData handData = null;

            pxcmStatus status = instance.EnableHand();
            handAnalysis = instance.QueryHand();

            if (status != pxcmStatus.PXCM_STATUS_NO_ERROR || handAnalysis == null)
            {
                   return;
            }

            handConfiguration = handAnalysis.CreateActiveConfiguration();
            if (handConfiguration == null)
            {
                instance.Close();
                instance.Dispose();
                return;
            }
            handData = handAnalysis.CreateOutput();
            if (handData == null)
            {
                handConfiguration.Dispose();
                instance.Close();
                instance.Dispose();
                return;
            }
            
            if (instance.Init(handler) == pxcmStatus.PXCM_STATUS_NO_ERROR)
            {
                PXCMCapture.DeviceInfo dinfo;
                PXCMCapture.DeviceModel dModel = PXCMCapture.DeviceModel.DEVICE_MODEL_F200;
                PXCMCapture.Device device = instance.captureManager.device;
                if (device != null)
                {
                    device.QueryDeviceInfo(out dinfo);
                    dModel = dinfo.model;
                }

                if (handConfiguration != null)
                {
                    PXCMHandData.TrackingModeType trackingMode = PXCMHandData.TrackingModeType.TRACKING_MODE_FULL_HAND;

                    handConfiguration.SetTrackingMode(trackingMode);

                    handConfiguration.EnableAllAlerts();
                    handConfiguration.EnableSegmentationImage(true);
                    bool isEnabled = handConfiguration.IsSegmentationImageEnabled();

                    handConfiguration.ApplyChanges();
                }

                int frameCounter = 0;
                int frameNumber = 0;

                if (handConfiguration != null)
                {
                    handConfiguration.DisableAllGestures();
                    var map = _form.ReturnDGVGestures();

                    handConfiguration.DisableAllGestures();

                    foreach (var row in map)
                    {
                        if (row.Key == "-") continue;
                        string curs = row.Key.Split('_')[0];
                        if (curs == "cursor") continue;
                        handConfiguration.EnableGesture(row.Key, true);
                    }

                    handConfiguration.ApplyChanges();
                }

                while (!_form.stop)
                {
                    if (instance.AcquireFrame(true) < pxcmStatus.PXCM_STATUS_NO_ERROR)
                    {
                        break;
                    }

                    frameCounter++;

                    if (instance.IsConnected())
                    {
                        PXCMCapture.Sample sample;
                        sample = instance.QueryHandSample();

                        if (sample != null && sample.depth != null)
                        {
                            frameNumber = liveCamera ? frameCounter : instance.captureManager.QueryFrameIndex();
                            if (handData != null)
                            {
                                handData.Update();

                                DisplayGesture(handData, frameNumber);
                                DisplayAlerts(handData, frameNumber);
                            }
                        }
                    }
                    instance.ReleaseFrame();
                }
            }

            if (session != null)
                session.Dispose();

            // Clean Up
            if (handData != null) handData.Dispose();
            if (handConfiguration != null) handConfiguration.Dispose();

            instance.Close();
            instance.Dispose();

            _form.Invoke(new del_VisibleInterface(
            delegate
            {
                _form.VisibleInterface();
            }));
        }
    }

    public delegate void del_VisibleInterface();
}
